# Ripple Trustline

This is my solution to the Ripple Trustline technical challenge.

I use BreezeHttpClient for REST API calls; it's the open source version of
the internal REST client library I wrote at Lending Club used throughout the
company. Unfortunately it's currently not in the Maven central repository,
so you must build it yourself; fortunately this is trivial to do:

```
git clone https://github.com/LendingClub/breeze-http-client
cd breeze-http-client
mvn clean install
```

Now you can build my Trustline solution the usual way:

```
git clone https://gitlab.com/cantara/trustline
cd trustline
mvn clean install
```

To run Alice's server:
 
```
cd trustline-server; mvn spring-boot:run -DtrustlineServer=alice
```
 
You can browse and test the API, which uses port 8080, via Swagger using any
browser: <http://localhost:8080/swagger-ui.html>.

Running and testing Bob's server is the same, except use `-DtrustlineServer=bob`;
it runs on port 8081 so the Swagger URL is <http://localhost:8081/swagger-ui.html>.

You can also run a simple command line read/eval/print loop shell to run manual
tests or run an automated integration test:

```
cd trustline-cli; mvn spring-boot:run -DtrustlineClient=alice
```

This client application will talk directly to Alice's port 8080 server (or
Bob's 8081 server with `-DtrustlineClient=bob`) using the REST API; if Alice
isn't a registered user on the server, this test app will automatically
register Alice with an initial balance of zero.

* `balance`: Get the current balance
* `balances`: Get the balances of all users on the server
* `send TO AMOUNT`: Send money to a user on the *other* server. For example
  `send bob 20` will transfer $20 from Alice's server to Bob's.
* `user USER`: Switches your current user; if `USER` doesn't exist on the server,
  it is registered first. The `balance` and `send` commands will then use this
  new user's account.
* `test NUM_TRANSFERS`: Run an integration test that registers two new, random
  users on each server, and executes `NUM_TRANSFERS` transfers between them,
  each time randomly choosing amounts and which user sends money to the other.

## Architecture

I follow a very traditional application structure via the following modules:

* `trustline-service`: Service layer, main class is TrustlineService.
* `trustline-client`: The TrustlineClient class, used for both user client to
  server communication, as well as server to server.
* `trustline-server`: Spring Boot server module, includes TrustlineController.
* `trustline-cli`: Command line interface.

Because the API is so simple, I didn't create Java REST model classes; those
might typically live in a separate model project so they are decoupled from
everything else and can be more easily shared with other projects or services.

Note that I combined "user client to server" functionality with "server to
server" due to the simplistic nature of this assigment. In a real app, the APIs
(and clients) for each would be different; you would never allow a client API
to credit its own user, yet that is possible in this design. Typically the APIs
would be segregated: different clients, controllers, projects, in addition to
authentication/access controls, both at the API and network level.

Amounts are handled as integers. A real application would use a special class
to represent money, abstracting away currency and basic operations, as we
cannot use doubles due to precision issues, and nonetheless need to also
represent currency and other attributes.

Error handling is extremely simple; no actionable error codes in exceptions
thrown; the API always returns HTTP 500 (e.g. no Spring error handlers to
return 404 if you credit a user that doesn't exist). Sending money will
"rollback" the transfer only if there is a remote error; in real life this is
very tricky, as it's essentially an issue of eventual consistency and the
double spending problem.

Input validation is very rudimentary; you can't send money to yourself, but the
receiving party must be on the other server and there is no check for that.
Transfer amounts must be positive, but there are no limit/overflow checks.

There are only unit tests for TrustlineService; the rest of the classes are
plumbing anyway. Usually you'd implement basic tests for TrustlineClient
(using mocks to verify the REST calls are what you'd expect), and possibly
WireMock/MockServer for the controller/server. Real integration tests would
be similar in that they talk to "live" servers in a non-prod environment,
possibly deployed with service stubs to third party services so those don't
need to be live.  

## BreezeHttpClient

To make the REST API calls, I used BreezeHttpClient. Breeze is an HTTP interface
that makes REST very easy to use; see TrustlineClient. It is the open source
version of the REST library I wrote at Lending Club. Highlights:

1. Dead simple fluent API; Breeze is so named because something as fundamental
   as a REST API should be a breeze to use.
2. REST *interface*, with multiple implementations. If you don't like the
   Spring RestTemplate implementation, you swap out to a different one very
   easily without changing your actual client code. You can write a new
   implementation in a couple hours or less depending on how much functionality
   you want to support.
3. Easy to configure and extend; it provides a decorator pattern that allows
   you to do just about anything. Default decorators exist for automatic
   retries and defaulting URLs.
4. Built-in: intelligent request logging, automatic JSON to POJO conversion
   (including generic types and error responses), different exception types
   depending on HTTP client/server errors, fluent forms, access to raw
   responses (headers, HTTP status, raw body) if you so desire, hundreds of
   integration tests for all features.

For more details, see https://github.com/LendingClub/breeze-http-client.

## Trustline Thoughts

There are many complexities that are obviously not addressed here; it was very
tempting to play around with some of them, but the complexity scales up quickly
beyond the scope of such a simple exercise.

I am wholly unfamiliar with the real Trustline product, so it was a lot of fun
to read up about it and think about how a real system would implement this.

There is the question of discovery: tracking trustline servers that come
online/offline and how they find each other, and what users are registered
where. This is a fairly complex topic in and of itself.

However the biggest problem (in my limited understanding) is how a real system
ripples balances to another user in the most efficient manner. Calculating the
graph traversals from one user to another can get quite complicated, especially
if you want to do this in real time. How do you handle eventual consistency
through all the balances that are being shifted over time? Does it work to
compute the path from user to user, and then start rippling, when the traversal
can stop in its tracks if a node dies, or the cost changes?

I am sure there are myriad other problems involved once you start throwing in
intermediary service fees, currency conversions, state/country regulations,
and the quirks of using a cryptocurrency in the middle. I look forward to
discussing these issues in person.
