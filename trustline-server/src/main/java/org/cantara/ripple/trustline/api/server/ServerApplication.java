package org.cantara.ripple.trustline.api.server;

import org.cantara.ripple.trustline.client.TrustlineClient;
import org.cantara.ripple.trustline.service.TrustlineService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.logging.Logger;

@SpringBootApplication
@Configuration
@ComponentScan(basePackages = {"org.cantara"})
@EnableSwagger2
public class ServerApplication {
    private static final Logger logger = Logger.getLogger("TrustlineController");

    public static final int ALICE_PORT = 8080;
    public static final int BOB_PORT = 8081;

    private static String trustlineServerName;

    public static void main(String[] args) {
        trustlineServerName = System.getProperty("trustlineServer");
        if ("alice".equals(trustlineServerName)) {
            System.setProperty("server.port", String.valueOf(ALICE_PORT));
        } else if ("bob".equals(trustlineServerName)) {
            System.setProperty("server.port", String.valueOf(BOB_PORT));
        } else {
            throw new IllegalStateException("unrecognized trustlineServer=" + trustlineServerName + ", must be alice or bob");
        }
        SpringApplication.run(ServerApplication.class, args);
        logger.info("Server " + trustlineServerName + " started");
    }

    @Bean
    public TrustlineService trustlineService() {
        String otherUrl = "http://localhost:";
        if (trustlineServerName.equals("alice")) {
            otherUrl += BOB_PORT;
        } else {
            otherUrl += ALICE_PORT;
        }
        return new TrustlineService(new TrustlineClient(otherUrl));
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("org.cantara.ripple.trustline.api.controller"))
                .paths(PathSelectors.any())
                .build();
    }
}
