package org.cantara.ripple.trustline.api.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.cantara.ripple.trustline.service.TrustlineService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;
import java.util.logging.Logger;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class TrustlineController {
    private static final Logger logger = Logger.getLogger("TrustlineController");

    @Resource
    private TrustlineService service;

    @ApiOperation("Register a new user with this trustline server.")
    @RequestMapping(value = "/register/{user}", method = POST)
    public void register(@PathVariable String user) {
        logger.info("register user=" + user);
        service.register(user);
    }

    @ApiOperation("Get the current balance for an existing user.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User's current balance.", response = Integer.class),
            @ApiResponse(code = 500, message = "User is not registered on this server.")
    })
    @RequestMapping(value = "/balance/{user}", method = GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public int balance(@PathVariable String user) {
        logger.info("received balance user=" + user);
        int balance = service.balance(user);
        logger.info("balance=" + balance);
        return balance;
    }

    @ApiOperation("Get the balances for all users registered on this trustline server.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Map of users and their current balances."),
    })
    @RequestMapping(value = "/balances", method = GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Integer> balances() {
        Map<String, Integer> balances = service.balances();
        logger.info("balances=" + balances);
        return balances;
    }

    @ApiOperation("Send money from one user on this server to a user on another trustline server.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User's balance after balance transfer.", response = Integer.class),
            @ApiResponse(code = 500, message = "One of the users is not registered on the corresponding server.")
    })
    @RequestMapping(
            value = "/send/{from}/{to}",
            method = POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public int send(@PathVariable String from, @PathVariable String to, @RequestBody int amount) {
        logger.info("received send from=" + from + " to=" + to + " amount=" + amount);
        int balance = service.send(from, to, amount);
        logger.info("new balance=" + balance);
        return balance;
    }

    @ApiOperation("Credit a user on this server by a given amount.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User's balance after the credit.", response = Integer.class),
            @ApiResponse(code = 500, message = "User is not registered on this server.")
    })
    @RequestMapping(
            value = "/credit/{user}",
            method = POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public int credit(@PathVariable String user, @RequestBody int amount) {
        logger.info("received credit user=" + user + " amount=" + amount);
        int balance = service.credit(user, amount);
        logger.info("new balance=" + balance);
        return balance;
    }
}
