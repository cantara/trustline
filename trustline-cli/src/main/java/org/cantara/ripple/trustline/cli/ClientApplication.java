package org.cantara.ripple.trustline.cli;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientApplication {
    public static void main(String[] args) {
        String client = System.getProperty("trustlineClient");
        if ("alice".equals(client) || "bob".equals(client)) {
            new CommandLineRepl(client).readEvalPrintLoop();
        } else {
            throw new IllegalStateException("unrecognized trustlineClient=" + client + ", should be alice or bob");
        }
    }
}
