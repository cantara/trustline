package org.cantara.ripple.trustline.cli;

import org.cantara.ripple.trustline.client.TrustlineClient;

import java.util.Scanner;

public class CommandLineRepl {
    private String user;
    private final TrustlineClient client;

    public CommandLineRepl(String user) {
        this.user = user;

        String url = "http://localhost:";
        if (user.equals("alice")) {
            url += 8080;
        } else if (user.equals("bob")) {
            url += 8081;
        } else {
            throw new IllegalStateException("invalid user=" + user);
        }
        this.client = new TrustlineClient(url);
        Integer balance = client.balances().get(user);
        if (balance == null) {
            client.register(user);
        }
    }

    public void readEvalPrintLoop() {
        Scanner scanner = new Scanner(System.in);
        prompt();
        while (scanner.hasNextLine()) {
            String[] tokens = scanner.nextLine().split(" ");
            String command = tokens[0];
            try {
                switch (command) {
                    case "balance":
                        System.out.println("Your current balance is " + client.balance(user));
                        break;
                    case "balances":
                        System.out.println("Balances at the trustline server:");
                        client.balances().forEach((key, value) -> System.out.println(key + ": " + value));
                        break;
                    case "send":
                        String to = tokens[1];
                        int amount = Integer.parseInt(tokens[2]);
                        System.out.println("Your new balance is " + client.send(user, to, amount));
                        break;
                    case "user":
                        String newUser = tokens[1];
                        if (!client.balances().containsKey(newUser)) {
                            client.register(newUser);
                        }
                        System.out.println("You are now " + newUser + " with balance " + client.balance(newUser));
                        user = newUser;
                        break;
                    case "test":
                        int numTransfers = Integer.parseInt(tokens[1]);
                        new IntegrationTest().test(numTransfers);
                        break;
                    case "exit":
                        System.out.println("Thank you for using trustline!");
                        return;
                    default:
                        System.out.println("I didn't get that. Please try again.");
                }
            } catch (Exception e) {
                System.out.println(e + " executing command");
            }
            prompt();
        }
    }

    private void prompt() {
        System.out.println();
        System.out.println("You are running as user " + user + ", your current balance is " + client.balance(user) + ".");
        System.out.println("Available commands are:");
        System.out.println("    balance: show the balance for the current user");
        System.out.println("    balances: show balances for all users on the server");
        System.out.println("    send USER AMOUNT: transfer money from the current user to USER on the other server");
        System.out.println("    user USER: switch the current user");
        System.out.println("    test NUM_TRANSFERS: run integration test with two new random users");
        System.out.println("    exit: money is a means to an end; go enjoy it!");
        System.out.print("What would you like to do? ");
    }
}
