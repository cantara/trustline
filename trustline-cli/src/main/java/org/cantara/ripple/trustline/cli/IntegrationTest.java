package org.cantara.ripple.trustline.cli;

import org.cantara.ripple.trustline.client.TrustlineClient;

import java.util.Random;

import static org.springframework.util.Assert.isTrue;

public class IntegrationTest {
    private TrustlineClient clientA = new TrustlineClient("http://localhost:8080");
    private TrustlineClient clientB = new TrustlineClient("http://localhost:8081");

    private final String userA;
    private final String userB;
    private final Random random = new Random();

    public IntegrationTest() {
        long time = System.currentTimeMillis();
        userA = "userA_" + time;
        userB = "userB_" + time;
    }

    public void test(int numTransfers) {
        System.out.println("registering " + userA + " and " + userB);
        clientA.register(userA);
        clientB.register(userB);
        isTrue(clientA.balance(userA) == 0, "userA should start with zero balance");
        isTrue(clientB.balance(userB) == 0, "userB should start with zero balance");

        int balanceA = 0, balanceB = 0;
        while (numTransfers-- > 0) {
            final String from, to;
            final TrustlineClient client;
            final int expectedBalanceA, expectedBalanceB;
            int amount = (random.nextInt(10) + 1) * 10;
            if (random.nextBoolean()) {
                from = userA;
                to = userB;
                client = clientA;
                expectedBalanceA = balanceA - amount;
                expectedBalanceB = balanceB + amount;
            } else {
                from = userB;
                to = userA;
                client = clientB;
                expectedBalanceA = balanceA + amount;
                expectedBalanceB = balanceB - amount;
            }
            System.out.println("sending " + amount + " from " + from + " to " + to);
            client.send(from, to, amount);

            balanceA = clientA.balance(userA);
            balanceB = clientB.balance(userB);
            System.out.println(userA + " balance: " + balanceA);
            System.out.println(userB + " balance: " + balanceB);
            isTrue(balanceA == expectedBalanceA, "userA should have balance " + expectedBalanceA);
            isTrue(balanceB == expectedBalanceB, "userB should have balance " + expectedBalanceB);
        }
    }
}
