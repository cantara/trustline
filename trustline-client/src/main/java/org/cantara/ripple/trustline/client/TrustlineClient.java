package org.cantara.ripple.trustline.client;

import org.lendingclub.http.breeze.client.BreezeHttpClient;
import org.lendingclub.http.breeze.client.BreezeHttpType;
import org.lendingclub.http.breeze.client.decorator.EndpointDecorator;
import org.lendingclub.http.breeze.client.decorator.retry.RetryDecorator;
import org.lendingclub.http.breeze.client.exception.BreezeHttpException;
import org.lendingclub.http.breeze.client.impl.resttemplate.builder.BreezeHttpRestTemplateClientBuilder;

import java.util.Map;

/**
 * REST client for a trustline. This is a thin client wrapper that manages
 * the HTTP REST calls, but doesn't provide any additional logic.
 *
 * All calls can throw BreezeHttpException; the caller is responsible for knowing
 * what to do with them. If the actual exception is of type
 * BreezeHttpResponseException, it means there was an actual response from the
 * server; the exception includes the request, response, and HTTP status code.
 */
public class TrustlineClient {
    /** HTTP REST client to make the underlying calls. */
    private BreezeHttpClient breeze;

    /** Create new client that talks to a trustline server at the given URL. */
    public TrustlineClient(String url) {
        this.breeze = new BreezeHttpRestTemplateClientBuilder()
                // Retry after 10ms, then 20ms, 50ms, then gives up and throws exception
                .withDecorator(new RetryDecorator(10, 20, 50))
                // Default all requests to the given root URL
                .withDecorator(new EndpointDecorator(url, "trustline"))
                // Convert error responses to string; real API error responses could be mapped to a Java class
                .withErrorResponseClass(String.class)
                .build();
    }

    /**
     * Register the given user.
     *
     * @param user user to register
     * @throws BreezeHttpException if any error occurs
     */
    public void register(String user) throws BreezeHttpException {
        breeze.request()
                .name("register")
                .path("/register/{user}")
                .pathVariable("user", user)
                .post(Void.class);
    }

    /**
     * Get the balance for the user.
     *
     * @param user the name of the user
     * @return current balance
     * @throws BreezeHttpException if any error occurs
     */
    public int balance(String user) throws BreezeHttpException {
        return breeze.request()
                .name("balance")
                .path("/balance/{user}")
                .pathVariable("user", user)
                .get(Integer.class);
    }

    /**
     * Get balances for all users registered at the server.
     *
     * @return map of user to balance
     * @throws BreezeHttpException if any error occurs
     */
    public Map<String, Integer> balances() throws BreezeHttpException {
        return breeze.request()
                .name("balances")
                .path("/balances")
                .get(new BreezeHttpType<Map<String, Integer>>() {});
    }

    /**
     * Send money from a user at this client's server to a user at the other trustline server.
     *
     * @param from user to send money from
     * @param to user to send money to
     * @param amount amount to send
     * @return source user's new balance after transfer
     * @throws BreezeHttpException if any error occurs
     */
    public int send(String from, String to, int amount) throws BreezeHttpException {
        return breeze.request()
                .name("send")
                .path("/send/{from}/{to}")
                .pathVariable("from", from)
                .pathVariable("to", to)
                .post(Integer.class, amount);
    }

    /**
     * Credit user on this server.
     *
     * @param user user to credit
     * @param amount the amount to adjust their balance
     * @throws BreezeHttpException if any error occurs
     */
    public void credit(String user, int amount) throws BreezeHttpException {
        breeze.request()
                .name("credit")
                .path("/credit/{user}")
                .pathVariable("user", user)
                .post(Void.class, amount);
    }
}
