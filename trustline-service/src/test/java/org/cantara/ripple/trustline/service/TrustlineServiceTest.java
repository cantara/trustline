package org.cantara.ripple.trustline.service;

import org.cantara.ripple.trustline.client.TrustlineClient;
import org.cantara.ripple.trustline.exception.TrustlineException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.lendingclub.http.breeze.client.BreezeHttpResponse;
import org.lendingclub.http.breeze.client.exception.BreezeHttpResponseException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Unit tests for TrustlineService.
 *
 * Note that ideally we'd have error codes in TrustlineException which we
 * could validate in the tests rather than just blindly passing for any
 * TrustlineException.
 */
@RunWith(MockitoJUnitRunner.class)
public class TrustlineServiceTest {
    @Mock
    private TrustlineClient client;

    @InjectMocks
    private TrustlineService service;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(TrustlineServiceTest.class);
        service.register("raul");
    }

    @Test
    public void register() {
        assertEquals(0, service.balance("raul"));
    }

    @Test(expected = TrustlineException.class)
    public void register_alreadyRegistered() {
        service.register("raul");
    }

    @Test
    public void send() {
        assertEquals(-10, service.send("raul", "bob", 10));
        verify(client, times(1)).credit("bob", 10);
    }

    @Test(expected = TrustlineException.class)
    public void send_userDoesNotExist() {
        service.send("fred", "bob", 10);
    }

    @Test(expected = TrustlineException.class)
    public void send_sendToSelf() {
        service.send("raul", "raul", 10);
    }

    @Test(expected = TrustlineException.class)
    public void send_sendPositiveAmount() {
        service.send("raul", "bob", 0);
    }

    @Test
    public void send_httpError() {
        BreezeHttpResponseException error = new BreezeHttpResponseException(
                "fake error",
                null,
                new BreezeHttpResponse<>()
        );
        doThrow(error).when(client).credit("bob", 100);
        try {
            service.send("raul", "bob", 100);
            fail("client mock should have thrown error");
        } catch (TrustlineException e) {
            // User's balance should not have changed if we couldn't transfer to other party
            assertEquals(error, e.getCause());
            assertEquals(0, client.balance("raul"));
        }
    }

    @Test
    public void credit() {
        assertEquals(0, service.balance("raul"));
        service.credit("raul", 10);
        assertEquals(10, service.balance("raul"));
    }

    @Test(expected = TrustlineException.class)
    public void credit_userDoesNotExist() {
        service.credit("fred", 10);
    }

    @Test(expected = TrustlineException.class)
    public void credit_sendPositiveAmount() {
        service.credit("raul", 0);
    }
}
