package org.cantara.ripple.trustline.service;

import org.cantara.ripple.trustline.client.TrustlineClient;
import org.cantara.ripple.trustline.exception.TrustlineException;
import org.lendingclub.http.breeze.client.exception.BreezeHttpResponseException;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Trustline service that manages the trustline of users on the current
 * server, and can transfer money to a user on another server.
 */
public class TrustlineService {
    /** Map of users to their balances. */
    private Map<String, Integer> balances = new HashMap<>();

    /** Client to communicate to other trustline servers. */
    private final TrustlineClient client;

    /**
     * Create new service, which can talk to only one other trustline server
     * using the preconfigured client instance.
     *
     * @param client client which can talk to the other trustline server
     */
    public TrustlineService(TrustlineClient client) {
        this.client = client;
    }

    /**
     * Register a user on this server. Their initial balance will be 0.
     *
     * @param user name of user
     * @throws TrustlineException if the user is already registered
     */
    public void register(String user) throws TrustlineException {
        if (balances.containsKey(user)) {
            throw new TrustlineException("user=" + user + " already registered");
        }
        balances.put(user, 0);
    }

    /**
     * Return the current balance for a user on this server.
     *
     * @param user name of the user
     * @return current balance
     * @throws TrustlineException if the user does not exist on this server
     */
    public int balance(String user) throws TrustlineException {
        Integer balance = balances.get(user);
        if (balance == null) {
            throw new TrustlineException("user=" + user + " not found in this trustline");
        }
        return balance;
    }

    /**
     * Returns a map of the current users and their balances.
     *
     * @return map of user to balance
     */
    public Map<String, Integer> balances() {
        return balances;
    }

    /**
     * Send money from a user on this server to a user on the other trustline server.
     *
     * @param from user on this server to transfer money from
     * @param to user on the other trustline server to which money is being sent
     * @param amount the amount to transfer
     * @return the from user's new balance after the transfer
     * @throws TrustlineException if either user does is not already registered on the appropriate server;
     *                            no money is transferred if the remote call to the other server fails;
     *                            or the transfer amount is not positive
     */
    public int send(String from, String to, int amount) throws TrustlineException {
        if (amount <= 0) {
            throw new TrustlineException("amount to send must be positive");
        }
        if (Objects.equals(from, to)) {
            throw new TrustlineException("from and to users are the same");
        }
        if (!balances.containsKey(from)) {
            throw new TrustlineException("user from=" + from + " does not exist in this trustline");
        }
        try {
            int newBalance = balances.get(from) - amount;
            balances.put(from, newBalance);
            client.credit(to, amount);
            return newBalance;
        } catch (BreezeHttpResponseException e) {
            balances.put(from, balances.get(from) + amount);
            throw new TrustlineException("remote error sending from=" + from + " to=" + to + " amount=" + amount, e);
        } catch (Exception e) {
            throw new TrustlineException("error sending from=" + from + " to=" + to + " amount=" + amount, e);
        }
    }

    /**
     * Credit a user on this server.
     *
     * @param user name of user to credit
     * @param amount the amount to add to their balance
     * @return new balance after credit is applied
     * @throws TrustlineException if the user does not already exist on this server,
     *                            or credit amount is not positive
     */
    public int credit(String user, int amount) throws TrustlineException {
        if (amount <= 0) {
            throw new TrustlineException("amount to credit must be positive");
        }
        if (!balances.containsKey(user)) {
            throw new TrustlineException("user=" + user + " does not exist in this trustline");
        }
        try {
            int newBalance = balances.getOrDefault(user, 0) + amount;
            balances.put(user, newBalance);
            return newBalance;
        } catch (Exception e) {
            throw new TrustlineException("error crediting user=" + user + " amount=" + amount, e);
        }
    }
}
