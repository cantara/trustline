package org.cantara.ripple.trustline.exception;

public class TrustlineException extends RuntimeException {
    public TrustlineException() {
    }

    public TrustlineException(String message) {
        super(message);
    }

    public TrustlineException(String message, Throwable cause) {
        super(message, cause);
    }

    public TrustlineException(Throwable cause) {
        super(cause);
    }
}
